import networkx as nx
import sys
import matplotlib.pyplot as plt
from sklearn import datasets
# from GraphUtil import GraphUtil
# import GraphUtil as GU
import graphutil.GraphUtil as GU
from networkx.algorithms.flow import shortest_augmenting_path

d = datasets.load_iris()
# d = datasets.load_diabetes()
G = nx.MultiGraph() 
features = d.feature_names
for i in range(len(d.data)-1):
    item = d.data[i]
    # G.add_node(i,sepalLength=item[0], sepalWidth=item[1],petalLength=item[2],petalWidth=item[3])
    G.add_node(i)
    for j in range(len(features)):
        G._node[i][features[j]] = item[j]

total = G.number_of_nodes()
# # total = 2
for i in range(total):
    for j in range(i, total):
        if i != j:
            itemA = G._node[i]
            itemB = G._node[j]
            attnumb = len(itemA)
        
            for key in itemA:
                # print(i, j, itemA[key])
                if(itemA[key] == itemB[key]):
                    val = itemA[key]
                    G.add_edge(i, j, key)
                pass
def minCut():
    pass

def highly_connected(G, E):
    return len(E) > len(G.nodes) / 2
    pass

def HCS(G):
    E = nx.minimum_edge_cut(G, flow_func=shortest_augmenting_path)
    if not highly_connected(G,E):
        for e in E:
            G.remove_edge(*e)
            pass
        # sub_graph = nx.connected_component_subgraphs(G)
        # print(len(sub_graph))
        print(nx.number_connected_components(G))
        print(nx.is_connected(G))
        print(len(E))
    else:
        pass
    return G

        
        

HCS(G)
nx.draw(G)
plt.show()
            