import hcs as hcs
import networkx as nx


G = hcs.create_example_graph()
# label = hcs.labelled_HCS(G)/.
# # print(label)
# print(G.nodes())
# print(G.get_edge_data(1,3))
E = nx.algorithms.connectivity.minimum_edge_cut(G)
print(E)
# l =  G.adjacency()
# for i in l:
    # print(i)
import sys

import matplotlib.pyplot as plt
import networkx as nx

G = nx.grid_2d_graph(5, 5)  # 5x5 grid

# print the adjacency list
for line in nx.generate_adjlist(G):
    print(line)
# write edgelist to grid.edgelist
nx.write_edgelist(G, path="grid.edgelist", delimiter=":")
# read edgelist from grid.edgelist
H = nx.read_edgelist(path="grid.edgelist", delimiter=":")

nx.draw(H)
plt.show()