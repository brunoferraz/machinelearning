import networkx as nx
import sys
import matplotlib.pyplot as plt

G = nx.Graph()
G.add_node(1)
G.add_nodes_from([2,3])

H = nx.path_graph(10)
G.add_nodes_from(H)
G.add_edge(1,2)
G.add_edge(1,3)
G.add_edge(1, "A")
G.add_edge(2,3)
e = (2,3)
G.add_edge(*e)
G.add_node("A")
# print(e)
print(G.nodes)
print(G.edges)

print(G.adj[1])
print(G.nodes.data())
# list(G.nodes)
G = nx.petersen_graph()
plt.subplot(121)

nx.draw(G, with_labels=True, font_weight='bold')
plt.subplot(122)

nx.draw_shell(G, nlist=[range(5, 10), range(5)], with_labels=True, font_weight='bold')
plt.show()